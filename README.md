# lightweight tmx parser

This is a script i made to parse tmx files without loading them into memory in their entirety. 
The script only sees one line at the time, instead of the entire file, drastically reducing the memory needs when parsing huge tmx files.

The translation pairs are stored in a list containing dicts representing each pair. 