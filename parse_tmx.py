def tmx_to_pairs(filename):
    pairs = []
    with open(filename, "r") as f:
        readingpair = False
        sentpair = []
        for line in f:
            if "<tu>" in line:
                readingpair = True
                sentpair = []
            if readingpair and "<tuv" in line:
                sentence = line.split("<seg>")[1].split("</seg>")[0]
                sentpair.append(sentence)
            if "</tu>" in line:
                readingpair = False
                pairs.append({
                        "sent1": sentpair[0],
                        "sent2": sentpair[1]
                    })

    return pairs